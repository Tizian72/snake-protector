using UnityEngine;
using UnityEngine.UI;


public class ShootingController : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform firePoint;
    public float projectileForce = 10f;
    public LayerMask enemyLayer;
    public float explosionRadius = 1f;
    public int maxBullets = 10; // L�mite m�ximo de balas
    public KeyCode reloadKey = KeyCode.R; // Tecla para recargar
    public float reloadTime = 3f; // Tiempo de recarga en segundos
    public bool powerUpActive = false; // Indica si el Power-Up est� activo

    public Text bulletCountText; // Referencia al texto de las balas restantes
    public Text reloadTimerText; // Referencia al texto del temporizador de recarga

    private int currentBullets; // Balas restantes actualmente
    private bool isReloading = false; // Indica si est� en proceso de recarga
    private float reloadTimer; // Temporizador de recarga

    void Start()
    {
        currentBullets = maxBullets;

        // Actualizar el texto de las balas restantes al inicio del juego
        UpdateBulletCountText();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }

        if (Input.GetKeyDown(reloadKey) && currentBullets < maxBullets && !isReloading)
        {
            StartReload();
        }

        // Actualizar el temporizador de recarga
        UpdateReloadTimer();
    }

    void Shoot()
    {
        if (currentBullets <= 0 || isReloading)
        {
            Debug.Log("No puedes disparar en este momento.");
            return;
        }

        GameObject projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
        Rigidbody rb = projectile.GetComponent<Rigidbody>();
        rb.AddForce(firePoint.forward * projectileForce, ForceMode.VelocityChange);

        Collider[] hitEnemies = Physics.OverlapSphere(projectile.transform.position, explosionRadius, enemyLayer);
        foreach (Collider enemy in hitEnemies)
        {
            // Aqu� puedes realizar cualquier acci�n con los enemigos golpeados
            Debug.Log("Golpeaste a un enemigo: " + enemy.name);
        }

        Destroy(projectile, 2f);

        currentBullets--;

        // Actualizar el texto de las balas restantes despu�s de disparar
        UpdateBulletCountText();
    }

    void StartReload()
    {
        isReloading = true;
        reloadTimer = reloadTime;

        // Actualizar el texto del temporizador de recarga
        UpdateReloadTimer();
    }

    void UpdateReloadTimer()
    {
        if (isReloading)
        {
            reloadTimer -= Time.deltaTime;

            // Actualizar el texto del temporizador de recarga
            reloadTimerText.text = "Recargando: " + Mathf.CeilToInt(reloadTimer).ToString();

            if (reloadTimer <= 0f)
            {
                FinishReload();
            }
        }
        else
        {
            reloadTimerText.text = "";
        }
    }

    void FinishReload()
    {
        currentBullets = maxBullets;
        isReloading = false;

        // Actualizar el texto de las balas restantes despu�s de recargar
        UpdateBulletCountText();
    }

    // Funci�n para activar el Power-Up y cambiar el estilo de disparo
    void ActivatePowerUp()
    {
        powerUpActive = true;
        // Agrega aqu� el c�digo para cambiar el estilo de disparo cuando el Power-Up est� activo
    }

    void UpdateBulletCountText()
    {
        // Actualizar el texto de las balas restantes
        bulletCountText.text = "Balas: " + currentBullets.ToString();
    }
}