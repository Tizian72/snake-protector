using UnityEngine;

public class LanzadorProyectiles : MonoBehaviour
{
    public GameObject proyectilPrefab; // Prefab del proyectil
    public Transform puntoLanzamiento; // Punto desde donde se lanza el proyectil
    public float intervaloLanzamiento = 1f; // Intervalo de tiempo entre lanzamientos
    public float duracionProyectil = 5f; // Duraci�n en segundos antes de que el proyectil desaparezca
    public float velocidadProyectil = 10f; // Velocidad de movimiento del proyectil

    private float tiempoUltimoLanzamiento; // Tiempo del �ltimo lanzamiento

    void Update()
    {
        // Verificar si ha pasado el tiempo suficiente desde el �ltimo lanzamiento
        if (Time.time - tiempoUltimoLanzamiento >= intervaloLanzamiento)
        {
            LanzarProyectil();
            tiempoUltimoLanzamiento = Time.time; // Actualizar el tiempo del �ltimo lanzamiento
        }
    }

    // M�todo para lanzar un proyectil
    private void LanzarProyectil()
    {
        // Instanciar el proyectil desde el prefab en el punto de lanzamiento
        GameObject proyectilObject = Instantiate(proyectilPrefab, puntoLanzamiento.position, puntoLanzamiento.rotation);

        // Obtener el componente del script Proyectil del proyectil instanciado
        Proyectil proyectil = proyectilObject.GetComponent<Proyectil>();

        // Pasar la referencia del script MovimientoPersonaje al proyectil
        proyectil.SetMovimientoPersonaje(FindObjectOfType<MovimientoPersonaje>());

        // Configurar la velocidad de movimiento del proyectil
        Rigidbody proyectilRigidbody = proyectilObject.GetComponent<Rigidbody>();
        proyectilRigidbody.velocity = puntoLanzamiento.forward * velocidadProyectil;

        // Destruir el proyectil despu�s de la duraci�n especificada
        Destroy(proyectilObject, duracionProyectil);
    }
}
