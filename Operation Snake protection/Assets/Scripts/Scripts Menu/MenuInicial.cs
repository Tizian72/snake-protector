using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour
{
    public void StartGame()
    {
        // Cargar la escena del juego
        SceneManager.LoadScene("GameScene");
    }

    public void ActivateExtra()
    {
        // Cargar una escena adicional, como un nuevo nivel
        SceneManager.LoadScene("ExtraScene");
    }

    public void QuitGame()
    {
        // Salir del juego
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
