using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PowerUp : MonoBehaviour
{
    public Transform shootPoint;  // Punto de disparo de las balas
    public GameObject bulletPrefab;  // Prefab de las balas
    public int maxBullets = 100;  // N�mero m�ximo de balas en la r�faga
    public float bulletSpeed = 10f;  // Velocidad de las balas
    public float fireRate = 0.1f;  // Cadencia de disparo
    public float bulletLifetime = 5f;  // Tiempo de vida de las balas
    public KeyCode activationKey = KeyCode.F;  // Tecla para activar el Power Up
    public Text bulletCountText;  // Texto para mostrar la cantidad de balas restantes

    private int currentBullets;  // Balas restantes en la r�faga
    private bool powerUpActive = false;  // Indica si el Power Up est� activo
    private Coroutine deactivateCoroutine;  // Referencia al temporizador de desactivaci�n
    private GameObject objectToDestroy;  // Objeto a destruir al finalizar el Power Up

    private void Start()
    {
        currentBullets = maxBullets;
        bulletCountText.text = currentBullets.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(activationKey) && powerUpActive == false)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, 1f);
            foreach (Collider collider in colliders)
            {
                if (collider.CompareTag("Player"))
                {
                    ActivatePowerUp();
                    break;
                }
            }
        }
    }

    private void ActivatePowerUp()
    {
        StartCoroutine(GatlingBulletsRoutine());
        StartDeactivateTimer();
    }

    private IEnumerator GatlingBulletsRoutine()
    {
        powerUpActive = true;

        while (currentBullets > 0)
        {
            GameObject bullet = Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);
            Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();
            bulletRigidbody.velocity = shootPoint.forward * bulletSpeed;

            currentBullets--;
            bulletCountText.text = currentBullets.ToString();

            Destroy(bullet, bulletLifetime);

            yield return new WaitForSeconds(fireRate);
        }

        powerUpActive = false;
        bulletCountText.text = "";

        if (objectToDestroy != null)
        {
            Destroy(objectToDestroy);
        }

        Destroy(gameObject);
    }

    private void StartDeactivateTimer()
    {
        deactivateCoroutine = StartCoroutine(DeactivatePowerUp());
    }

    private void StopDeactivateTimer()
    {
        if (deactivateCoroutine != null)
        {
            StopCoroutine(deactivateCoroutine);
        }
    }

    private IEnumerator DeactivatePowerUp()
    {
        yield return new WaitForSeconds(maxBullets * fireRate);
        StopDeactivateTimer();  // Detener el temporizador antes de destruir el Power Up
        powerUpActive = false;
        bulletCountText.text = "";
        if (objectToDestroy != null && objectToDestroy.gameObject == gameObject)
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        StopDeactivateTimer();  // Detener el temporizador si el Power Up se destruye
    }

    public void SetObjectToDestroy(GameObject obj)
    {
        objectToDestroy = obj;
    }
}