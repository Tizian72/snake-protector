using UnityEngine;

public class MovimientoPersonaje : MonoBehaviour
{
    public float movementSpeed = 5f; // Velocidad de movimiento del personaje
    public KeyCode jumpKey = KeyCode.Space; // Tecla de salto elegida por el jugador
    public float jumpForce = 5f; // Fuerza aplicada al saltar
    private bool isGrounded = true; // Indicador de si el personaje est� en el suelo

    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        // Mover el personaje horizontalmente
        transform.Translate(Vector3.forward * horizontalInput * movementSpeed * Time.deltaTime);

        // Verificar si se ha presionado la tecla de salto y el personaje est� en el suelo
        if (Input.GetKeyDown(jumpKey) && isGrounded)
        {
            // Aplicar fuerza vertical para simular el salto
            GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false; // El personaje ya no est� en el suelo despu�s de saltar
        }
    }

    // Detectar colisiones con el suelo
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true; // El personaje ha vuelto a tocar el suelo
        }
    }
}
