using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemyPrefabs;
    public Transform spawnPoint;
    public float speed = 5f;
    public float spawnInterval = 2f;

    private void Start()
    {
        // Iniciar el spawn de enemigos
        InvokeRepeating("SpawnEnemy", 0f, spawnInterval);
    }

    private void SpawnEnemy()
    {
        // Seleccionar aleatoriamente un prefab de enemigo de la lista
        GameObject randomEnemyPrefab = enemyPrefabs[Random.Range(0, enemyPrefabs.Length)];

        // Instanciar el enemigo en el punto de spawn
        GameObject enemy = Instantiate(randomEnemyPrefab, spawnPoint.position, spawnPoint.rotation);

        // Obtener la direcci�n hacia la que se mover� el enemigo (en este caso, la direcci�n hacia adelante)
        Vector3 moveDirection = spawnPoint.forward;

        // Obtener el componente EnemyMovement del enemigo y establecer la direcci�n y velocidad de movimiento
        EnemyMovement enemyMovement = enemy.GetComponent<EnemyMovement>();
        enemyMovement.SetMovement(moveDirection, speed);
    }
}