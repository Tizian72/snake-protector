using UnityEngine;

public class Proyectil : MonoBehaviour
{
    public float tiempoParalizar = 2f; // Tiempo en segundos para paralizar al jugador

    private bool paralizado = false; // Indicador de si el jugador est� paralizado
    private MovimientoPersonaje movimientoPersonaje; // Referencia al script MovimientoPersonaje

    // Asignar la referencia al script MovimientoPersonaje al instanciar el proyectil
    public void SetMovimientoPersonaje(MovimientoPersonaje movimientoPersonaje)
    {
        this.movimientoPersonaje = movimientoPersonaje;
    }

    // Detectar colisiones con el jugador
    private void OnCollisionEnter(Collision collision)
    {
        if (!paralizado && collision.gameObject.CompareTag("Player"))
        {
            paralizado = true; // Marcar al jugador como paralizado
            if (movimientoPersonaje != null)
            {
                // Desactivar el script MovimientoPersonaje temporalmente
                movimientoPersonaje.enabled = false;

                // Reactivar el script MovimientoPersonaje despu�s de cierto tiempo
                Invoke("ReactivateMovimientoPersonaje", tiempoParalizar);
            }

            // Destruir el proyectil
            Destroy(gameObject);
        }
    }

    // M�todo para reactivar el script MovimientoPersonaje
    private void ReactivateMovimientoPersonaje()
    {
        if (movimientoPersonaje != null)
        {
            // Reactivar el script MovimientoPersonaje
            movimientoPersonaje.enabled = true;
        }
        paralizado = false; // Restablecer el indicador de paralizado
    }
}

