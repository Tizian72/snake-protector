using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public Text textoGameOver;
    public string tagEnemigo; // El tag del enemigo que activar� el Game Over

    private bool gameOver = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(tagEnemigo) && !gameOver)
        {
            textoGameOver.gameObject.SetActive(true); // Mostrar el texto de Game Over
            gameOver = true;
        }
    }
}

