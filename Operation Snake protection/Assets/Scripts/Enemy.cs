using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static event System.Action OnEnemyEliminated;

    private void OnDestroy()
    {
        if (OnEnemyEliminated != null)
        {
            OnEnemyEliminated.Invoke();
        }
    }
}