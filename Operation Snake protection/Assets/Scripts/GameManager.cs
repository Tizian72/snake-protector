using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Text enemiesEliminatedText;
    public Text highScoreText;
    private int enemiesEliminated = 0;
    private int highScore = 0;
    public KeyCode resetKey = KeyCode.R;

    private bool isGamePaused = false;

    private void Start()
    {
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        highScoreText.text = "High Score: " + highScore.ToString();
    }

    private void OnEnable()
    {
        Enemy.OnEnemyEliminated += IncreaseEnemiesEliminated;
    }

    private void OnDisable()
    {
        Enemy.OnEnemyEliminated -= IncreaseEnemiesEliminated;
    }

    private void IncreaseEnemiesEliminated()
    {
        enemiesEliminated++;
        UpdateUI();

        if (enemiesEliminated > highScore)
        {
            highScore = enemiesEliminated;
            PlayerPrefs.SetInt("HighScore", highScore);
            highScoreText.text = "High Score: " + highScore.ToString();
        }
    }

    private void UpdateUI()
    {
        enemiesEliminatedText.text = "Enemigos eliminados: " + enemiesEliminated.ToString();
    }

    public void GameOver()
    {
        enemiesEliminated = 0;
        UpdateUI();
    }

    private void Update()
    {
        if (Input.GetKeyDown(resetKey))
        {
            RestartGame();
        }
    }

    public void RestartGame()
    {
        enemiesEliminated = 0;
        UpdateUI();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ResumeGame();
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        isGamePaused = true;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
        isGamePaused = false;
    }
}




