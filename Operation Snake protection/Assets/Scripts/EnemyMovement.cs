using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private Vector3 moveDirection;
    private float speed;
    public int maxBulletHits = 1; // N�mero m�ximo de balas que el enemigo puede recibir antes de destruirse
    private int bulletHits = 0; // Contador de balas recibidas

    public void SetMovement(Vector3 direction, float moveSpeed)
    {
        moveDirection = direction.normalized;
        speed = moveSpeed;
    }

    private void Update()
    {
        // Mover el enemigo en la direcci�n establecida a una velocidad determinada
        transform.Translate(moveDirection * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            // Incrementar el contador de balas recibidas
            bulletHits++;

            // Comprobar si el n�mero m�ximo de balas ha sido alcanzado
            if (bulletHits >= maxBulletHits)
            {
                // Destruir tanto el enemigo como la bala al alcanzar el n�mero m�ximo de balas
                Destroy(other.gameObject); // Destruir la bala
                Destroy(gameObject); // Destruir el enemigo
            }
            else
            {
                // Si a�n no se ha alcanzado el n�mero m�ximo de balas, solo destruir la bala
                Destroy(other.gameObject); // Destruir la bala
            }
        }
        else if (other.CompareTag("NPC"))
        {
            // Detener la partida al colisionar con un objeto de etiqueta "NPC"
            Time.timeScale = 0f;
            // Puedes agregar aqu� cualquier otra l�gica que desees ejecutar al detener la partida
        }
    }

}