using UnityEngine;
using UnityEngine.AI;

public class NPC : MonoBehaviour
{
    public Transform target; // El transform del jugador
    private NavMeshAgent navMeshAgent;

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        SetDestination();
    }

    void SetDestination()
    {
        if (target != null)
        {
            navMeshAgent.SetDestination(target.position);
        }
    }

    void Update()
    {
        SetDestination();
    }
}
