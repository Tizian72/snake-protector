using UnityEngine;

public class ThirdPersonController : MonoBehaviour
{
    public Transform cameraTransform;
    public float moveSpeed = 5f;
    public float rotationSpeed = 10f;
    public float jumpForce = 5f;
    public float wallClimbSpeed = 2f;
    public LayerMask wallLayer;

    private Rigidbody rb;
    private bool isGrounded;
    private bool isClimbing;
    private Vector3 initialCameraOffset;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialCameraOffset = cameraTransform.position - transform.position;
    }

    void Update()
    {
        if (!isClimbing)
        {
            // Obtener la entrada de movimiento del jugador
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");

            // Calcular la direcci�n de movimiento en relaci�n con la c�mara
            Vector3 movement = cameraTransform.forward * verticalInput + cameraTransform.right * horizontalInput;
            movement.y = 0f;
            movement.Normalize();

            // Rotar el personaje hacia la direcci�n de movimiento
            if (movement != Vector3.zero)
            {
                Quaternion toRotation = Quaternion.LookRotation(movement, Vector3.up);
                transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
            }

            // Mover el personaje en la direcci�n de movimiento
            rb.velocity = movement * moveSpeed + new Vector3(0f, rb.velocity.y, 0f);

            // Saltar
            if (isGrounded && Input.GetButtonDown("Jump"))
            {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
            }
        }
        else
        {
            // Movimiento en la superficie vertical (pared)
            float verticalInput = Input.GetAxis("Vertical");

            // Calcular la direcci�n de movimiento en relaci�n con la normal de la pared
            Vector3 wallNormal = transform.forward;
            Vector3 climbDirection = Vector3.Cross(wallNormal, Vector3.up).normalized;

            // Mover el personaje hacia arriba en la pared
            rb.velocity = climbDirection * verticalInput * wallClimbSpeed + new Vector3(0f, rb.velocity.y, 0f);
        }
    }

    void LateUpdate()
    {
        // Seguir al personaje con la c�mara
        Vector3 targetCameraPosition = transform.position + initialCameraOffset;
        cameraTransform.position = Vector3.Lerp(cameraTransform.position, targetCameraPosition, rotationSpeed * Time.deltaTime);
    }

    void OnCollisionEnter(Collision collision)
    {
        // Verificar si el personaje est� en el suelo
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        // Verificar si el personaje ha dejado de estar en el suelo
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // Verificar si el personaje est� en una superficie vertical (pared)
        if (wallLayer == (wallLayer | (1 << other.gameObject.layer)))
        {
            isClimbing = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        // Verificar si el personaje ha dejado la superficie vertical (pared)
        if (wallLayer == (wallLayer | (1 << other.gameObject.layer)))
        {
            isClimbing = false;
        }
    }
}


